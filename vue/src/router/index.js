// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LanguageView from '../views/LanguageView.vue'
import EditView from '../views/EditView.vue'
import ComponentView from '../views/ComponentView.vue'
import ProfileView from '../views/ProfileView.vue'
import ReviewView from '../views/ReviewView.vue'
import ReviewListView from '../views/ReviewListView.vue'
import Header from '../components/Header.vue'
import FilterHeader from '../components/FilterHeader.vue'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: HomeView,
        Header
      },
      props: {
        default: false,
        Header: {
          pages: [
            {
              name: 'Dashboard',
              link: { name: 'home' }
            }
          ]
        }
      }
    },
    {
      path: '/language/:lang',
      name: 'lang',
      components: {
        default: LanguageView,
        Header: FilterHeader
      },
      props: {
        default: false,
        Header: (route) => ({
          pages: [
            {
              name: route.params.lang,
              link: { name: 'lang', params: { lang: route.params.lang } }
            }
          ]
        })
      }
    },
    {
      path: '/language/:lang/component/:component',
      name: 'component',
      components: {
        default: ComponentView,
        Header
      },
      props: {
        default: false,
        Header: (route) => ({
          pages: [
            {
              name: route.params.lang,
              link: { name: 'lang', params: { lang: route.params.lang } }
            },
            {
              name: route.params.component,
              link: {
                name: 'component',
                params: {
                  lang: route.params.lang,
                  component: route.params.component
                }
              }
            }
          ]
        })
      }
    },
    {
      path: '/language/:lang/component/:component/edit/:file',
      name: 'edit',
      components: {
        default: EditView,
        Header: FilterHeader
      },
      props: {
        default: false,
        Header: (route) => ({
          pages: [
            {
              name: route.params.lang,
              link: { name: 'lang', params: { lang: route.params.lang } }
            },
            {
              name: route.params.component,
              link: {
                name: 'component',
                params: {
                  lang: route.params.lang,
                  component: route.params.component
                }
              }
            },
            {
              name: route.params.file,
              link: {
                name: 'edit',
                params: {
                  lang: route.params.lang,
                  component: route.params.component,
                  file: route.params.file
                }
              }
            }
          ]
        })
      }
    },
    {
      path: '/profile',
      name: 'profile',
      components: {
        default: ProfileView,
        Header
      },
      props: {
        default: false,
        Header: {
          pages: [
            {
              name: 'Profile',
              link: { name: 'profile' }
            }
          ]
        }
      }
    },
    {
      path: '/reviews',
      name: 'reviews',
      components: {
        default: ReviewListView,
        Header
      },
      props: {
        default: false,
        Header: {
          pages: [
            {
              name: 'Review',
              link: { name: 'reviews' }
            },
          ]
        }
      }
    },
    {
      path: '/review/:id',
      name: 'review',
      components: {
        default: ReviewView,
        Header
      },
      props: {
        default: false,
        Header: (route) => ({
          pages: [
            {
              name: 'Review',
              link: { name: 'reviews' }
            },
            {
              name: route.params.id,
              link: { name: 'review', params: { id: route.params.id } }
            },
          ]
        })
      }
    },
  ]
})

export default router
