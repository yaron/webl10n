// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

import { defineStore } from 'pinia'

export const useMainStore = defineStore('components', {
  state: () => {
    return {
      languages: {},
      changes: {},
      beforeSubmitOverlayOpen: false,
      changesToReview: [],
      user: false,
    }
  },
  actions: {
    async fetchComponents(lang) {
      const response = await fetch(`/api/v1/languages/${lang}`)
      this.languages[lang] = await response.json()
    },
    async fetchLanguages() {
      const response = await fetch(`/api/v1/languages/`)
      const langs = await response.json()
      for (let lang of langs) {
        this.languages[lang] = {}
      }
    },
    async fetchUser() {
      try {
        const response = await fetch(`/api/v1/userinfo`)
        this.user = await response.json()
      } catch (error) {
        this.user = false;
      }
    },
    async uploadChanges() {
      const response = await fetch(`/api/v1/changes/`, {
        method: 'POST',
        body: JSON.stringify(this.changes)
      })
      this.changes = []
      this.beforeSubmitOverlayOpen = false
    }
  }
})
