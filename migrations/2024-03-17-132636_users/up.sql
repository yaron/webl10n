-- SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
-- SPDX-License-Identifier: AGPL-3.0-or-later

CREATE TABLE users (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(255) NOT NULL,
  email VARCHAR(255)  NOT NULL,
  fullname VARCHAR(255) NOT NULL,
  developer BOOLEAN NOT NULL,
  picture VARCHAR(255) NOT NULL,

  UNIQUE(username),
  UNIQUE(email)
);
