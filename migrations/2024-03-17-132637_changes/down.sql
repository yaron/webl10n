-- SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
-- SPDX-License-Identifier: AGPL-3.0-or-later

DROP TABLE changes;
