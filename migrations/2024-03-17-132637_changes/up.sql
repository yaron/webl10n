-- SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
-- SPDX-License-Identifier: AGPL-3.0-or-later

CREATE TABLE changes (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  data TEXT NOT NULL,
  user_id INT NOT NULL,
  created_at DATETIME NOT NULL,
  modified_at DATETIME NOT NULL,

  FOREIGN KEY (user_id)
    REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
