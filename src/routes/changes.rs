// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::HashMap;
use rocket::response::status::{BadRequest, Created};
use rocket::serde::json::Json;
use serde::Serialize;

use crate::db::{self, changes};
use crate::models::changes::{ChangeEntry, Change};
use crate::models::response::MessageResponse;
use crate::models::users::User;

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub struct ChangeOutput {
    id: i32,
    files: HashMap<String, Vec<ChangeEntry>>,
    modified_at: chrono::NaiveDateTime,
    created_at: chrono::NaiveDateTime,
    user: User,
}

impl From<(Change, User)> for ChangeOutput {
    fn from((change, user): (Change, User)) -> Self {
        Self {
            id: change.id,
            modified_at: change.modified_at,
            created_at: change.created_at,
            files: serde_json::from_str(&change.data).expect("Valid json"),
            user,
        }
    }
}

#[get("/?<limit>&<after>")]
pub async fn find_change(db: db::Database, limit: Option<i64>, after: Option<i32>)
    -> Result<Json<Vec<ChangeOutput>>, BadRequest<Json<MessageResponse>>> {

    let mut limit: i64 = limit.unwrap_or(30);
    if limit > 100 {
        limit = 100;
    }

    let after: i32 = after.unwrap_or(0);

    match changes::find_change(db, limit, after).await {
        Ok(changes) => Ok(Json(changes.into_iter().map(|x| x.into()).collect())),
        Err(_) => Err(BadRequest(Json(MessageResponse {
            message: "Database error".to_string(),
        })))
    }
}

#[get("/<change_id>")]
pub async fn find_change_by_id(db: db::Database, change_id: i32) -> Result<Json<ChangeOutput>, BadRequest<Json<MessageResponse>>> {
    match changes::find_change_by_id(db, change_id).await {
        Ok(change) => Ok(Json(change.into())),
        Err(_) => Err(BadRequest(Json(MessageResponse {
            message: "Database error".to_string(),
        })))
    }
}

/// Create a change to be reviewed
#[post("/", data = "<input>")]
pub async fn post_change(
    db: db::Database,
    user: User,
    input: Json<HashMap<String, Vec<ChangeEntry>>>,
) -> Result<Created<Json<Change>>, BadRequest<Json<MessageResponse>>> {
    if input.is_empty() {
        return Err(BadRequest(Json(MessageResponse {
            message: "Empty input".to_string(),
        })));
    }

    match changes::insert_change(user, db, input).await {
        Ok(created) => {
            Ok(Created::new("/").body(Json(created)))
        }
        Err(_error) => {
            println!("{:?}", _error);
            Err(BadRequest(Json(MessageResponse {
                message: "Invalid input".to_string(),
            })))
        }
    }
}
