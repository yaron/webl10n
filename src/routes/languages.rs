// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::models::components;
use rocket::serde::json::Json;
use std::fs;
use std::io::{Read, Write};
use std::{fs::File};

/// Get the list of languages available in the system
#[get("/")]
pub fn index() -> Json<Vec<String>> {
    let paths = fs::read_dir("/home/carl/kde/src/l10n-support/").unwrap();

    let mut languages: Vec<String> = vec![];

    for path in paths {
        languages.push(path.as_ref().unwrap().file_name().into_string().unwrap());
    }
    Json(languages)
}

/// Get the stats for the given language
#[get("/<lang>")]
pub fn get(lang: &str) -> Json<components::Language> {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("webl10n").unwrap();

    let lang_path = xdg_dirs.place_config_file(format!("{lang}.json")).unwrap();
    if lang_path.exists() {
        let mut file = File::open(lang_path).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        let lang_stats: components::Language = serde_json::from_str(contents.as_str()).unwrap();
        Json(lang_stats)
    } else {
        let lang_stats = components::build_cache(lang);
        let mut file = File::create(lang_path).unwrap();
        file.write_all(serde_json::to_string(&lang_stats).unwrap().as_bytes());
        Json(lang_stats)
    }
}
