// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::models::users::User;
use rocket::serde::json::Json;

#[get("/userinfo")]
pub fn userinfo(user: User) -> Json<User> {
    Json(user)
}
