// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::{Context, Error};
use rocket::http::{Cookie, CookieJar, SameSite};
use rocket_oauth2::{OAuth2, TokenResponse};
use reqwest::header::{AUTHORIZATION};
use rocket::response::{Debug, Redirect};
use crate::models::users::{UserInfo, User};
use crate::db::Database;

#[get("/login/gitlab")]
pub fn gitlab_login_logged(_user: User) -> Redirect {
    Redirect::to("/")
}

#[get("/login/gitlab", rank = 2)]
pub fn gitlab_login(oauth2: OAuth2<User>, cookies: &CookieJar<'_>) -> Redirect {
    oauth2.get_redirect(cookies, &["openid", "email", "profile"]).unwrap()
}

#[get("/accounts/invent/login/callback")]
pub async fn gitlab_callback(
    db: Database,
    db2: Database,
    token: TokenResponse<User>,
    cookies: &CookieJar<'_>,
) -> Result<Redirect, Debug<Error>> {
    // Use the token to retrieve the user's Gitlab account information.
    let request = reqwest::Client::builder()
        .build()
        .context("failed to build reqwest client")?
        .get("https://invent.kde.org/oauth/userinfo")
        .header(AUTHORIZATION, format!("Bearer {}", token.access_token()));

    let user_info: UserInfo = request
        .send()
        .await
        .context("failed to complete request")?
        .json().await
        .context("failed to deserialize response")?;

    match crate::db::users::find_by_username(db, user_info.nickname.clone()).await {
        Ok(user) => {
            cookies.add_private(
                Cookie::build(("user", user.id.to_string()))
                    .same_site(SameSite::Lax)
                    .build()
            );
        },
        Err(err) => {
            println!("{:?}", err);
            match crate::db::users::insert_user(db2, user_info).await {
                Ok(user) => {
                    cookies.add_private(
                        Cookie::build(("user", user.id.to_string()))
                            .same_site(SameSite::Lax)
                            .build()
                    );
                },
                Err(err) => {
                    println!("{:?}", err);
                }
            }
        }
    }


    Ok(Redirect::to("/"))
}

#[get("/logout")]
pub fn logout(_user: User, cookies: &CookieJar<'_>) -> Redirect {
    if let Some(cookie) = cookies.get_private("user") {
        cookies.remove_private(cookie);
    }

    Redirect::to("/")
}
