// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::models::units;
use poreader::PoParser;
use rocket::serde::json::Json;

use std::{fs::File};

#[get("/<lang>/<component>/<file>")]
pub async fn get(lang: &str, component: &str, file: &str) -> Json<Vec<units::Unit>> {
    let file = File::open(format!(
        "/home/carl/kde/src/l10n-support/{}/summit/messages/{}/{}",
        lang, component, file
    ))
    .unwrap();

    let parser = PoParser::new();
    let reader = parser.parse(file).unwrap();

    let mut units: Vec<units::Unit> = vec![];
    for unit in reader {
        match unit {
            Ok(unit) => {
                units.push(units::Unit {
                    message: unit.message().into(),
                    context: unit.context().map(|s| s.to_string()),
                    locations: unit.locations().to_vec(),
                    comments: unit
                        .comments()
                        .iter()
                        .map(|c| c.comment().to_owned())
                        .collect(),
                    state: unit.state(),
                })
            },
            _ => {}
        }
    }

    Json(units)
}
