// @generated automatically by Diesel CLI.

diesel::table! {
    changes (id) {
        id -> Integer,
        data -> Text,
        user_id -> Integer,
        created_at -> Datetime,
        modified_at -> Datetime,
    }
}

diesel::table! {
    users (id) {
        id -> Integer,
        #[max_length = 255]
        username -> Varchar,
        #[max_length = 255]
        email -> Varchar,
        #[max_length = 255]
        fullname -> Varchar,
        developer -> Bool,
        #[max_length = 255]
        picture -> Varchar,
    }
}

diesel::joinable!(changes -> users (user_id));

diesel::allow_tables_to_appear_in_same_query!(
    changes,
    users,
);
