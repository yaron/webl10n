// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::db::Database;
use crate::models::users::{UserInfo, User};
use crate::schema::users;
use diesel::prelude::*;

pub async fn find(
    db: Database,
    user_id: i32
) -> Result<User, diesel::result::Error> {
    db.run(move |conn| {
        users::table
            .find(user_id)
            .first::<User>(conn)
    }).await
}

pub async fn find_by_username(
    db: Database,
    username: String
) -> Result<User, diesel::result::Error> {
    db.run(move |conn| {
        users::table
            .filter(users::username.eq(username))
            .first::<User>(conn)
    }).await
}

/// New user
#[derive(Debug, Insertable)]
#[diesel(table_name = users)]
struct NewUser {
    username: String,
    email: String,
    fullname: String,
    developer: bool,
    picture: String,
}

/// Insert a new user and return the created user object
pub async fn insert_user(
    db: Database,
    new_user: UserInfo
) -> Result<User, diesel::result::Error> {
    let user = NewUser {
        username: new_user.nickname,
        email: new_user.email,
        fullname: new_user.name,
        picture: new_user.picture,
        developer: new_user.groups.iter().any(|i| i== "teams/kde-developers"),
    };

    db.run(move |conn| {
        conn.transaction(|conn| {
            diesel::insert_into(users::table)
                .values(user)
                .execute(conn)?;

            users::table
                .order(users::id.desc())
                .select(User::as_select())
                .first(conn)
        })
    }).await
}
