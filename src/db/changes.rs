// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use diesel::prelude::*;
use std::collections::HashMap;
use chrono::{Utc};
use rocket::serde::json::Json;
use crate::models::users::User;

use crate::db;
use crate::models::changes::{Change, ChangeEntry};
use crate::schema::changes;
use crate::schema::users;

#[derive(Debug, Insertable)]
#[diesel(table_name = crate::schema::changes)]
struct NewChange {
    pub data: String,
    pub user_id: i32,
    pub created_at: chrono::NaiveDateTime,
    pub modified_at: chrono::NaiveDateTime,
}

pub async fn find_change(db: db::Database, limit: i64, after: i32) -> Result<Vec<(Change, User)>, diesel::result::Error>  {
    db.run(move |conn| {
        changes::table
            .inner_join(users::table)
            .filter(changes::id.gt(after))
            .limit(limit)
            .select((Change::as_select(), User::as_select()))
            .load::<(Change, User)>(conn)
    }).await
}

pub async fn find_change_by_id(db: db::Database, change_id: i32) -> Result<(Change, User), diesel::result::Error> {
    db.run(move |conn| {
        changes::table
            .inner_join(users::table)
            .filter(changes::id.eq(change_id))
            .select((Change::as_select(), User::as_select()))
            .first::<(Change, User)>(conn)
    }).await
}

/// Insert a new change and return the created change
pub async fn insert_change(
    user: User,
    db: db::Database,
    changes: Json<HashMap<String, Vec<ChangeEntry>>>,
) -> Result<Change, diesel::result::Error> {
    let change = NewChange {
        data: serde_json::to_string(&changes.into_inner()).unwrap(),
        created_at: Utc::now().naive_utc(),
        modified_at: Utc::now().naive_utc(),
        user_id: user.id,
    };

    db.run(move |conn| {
        conn.transaction(|conn| {
            diesel::insert_into(changes::table)
                .values(change)
                .execute(conn)?;

            changes::table
                .order(changes::id.desc())
                .select(Change::as_select())
                .first(conn)
        })
    }).await
}
