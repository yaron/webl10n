// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use poreader::{PoParser, State};
use rocket::serde::{Deserialize, Serialize};
use std::fs;
use std::ops::{Add, AddAssign};
use std::{fs::File, io::Result};

#[derive(Default, Serialize, Deserialize, Clone, Copy)]
#[serde(crate = "rocket::serde")]
pub struct TranslationStats {
    pub total: i32,
    pub untranslated: i32,
    pub fuzzy: i32,
    pub translated: i32,
}

impl Add for TranslationStats {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            total: self.total + other.total,
            untranslated: self.untranslated + other.untranslated,
            fuzzy: self.fuzzy + other.fuzzy,
            translated: self.translated + other.translated,
        }
    }
}

impl AddAssign for TranslationStats {
    fn add_assign(&mut self, other: Self) {
        *self = Self {
            total: self.total + other.total,
            untranslated: self.untranslated + other.untranslated,
            fuzzy: self.fuzzy + other.fuzzy,
            translated: self.translated + other.translated,
        }
    }
}

#[derive(Serialize, Deserialize, Default, Clone)]
#[serde(crate = "rocket::serde")]
pub struct PoFile {
    pub name: String,
    pub stats: TranslationStats,
}

#[derive(Serialize, Deserialize, Default, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Component {
    pub name: String,
    pub stats: TranslationStats,
    pub po_files: Vec<PoFile>,
}

#[derive(Serialize, Deserialize, Default)]
#[serde(crate = "rocket::serde")]
pub struct Language {
    pub name: String,
    pub components: Vec<Component>,
    pub stats: TranslationStats,
}

fn gather_stats(lang: &str, component: &str, file: &str) -> Result<TranslationStats> {
    let mut file_state = TranslationStats::default();
    let file = File::open(format!(
        "/home/carl/kde/src/l10n-support/{}/summit/messages/{}/{}",
        lang, component, file
    ))?;

    let parser = PoParser::new();
    let reader = parser.parse(file)?;

    for unit in reader {
        match unit {
            Ok(unit) => {
                match unit.state() {
                    State::Empty => file_state.untranslated += 1,
                    State::NeedsWork => file_state.fuzzy += 1,
                    State::Final => file_state.translated += 1,
                }
                file_state.total += 1;
            }
            _ => {}
        }
    }

    Ok(file_state)
}

pub fn build_cache(lang: &str) -> Language {
    let mut language = Language {
        name: lang.to_string(),
        components: vec![],
        stats: TranslationStats::default(),
    };

    let components = fs::read_dir(format!(
        "/home/carl/kde/src/l10n-support/{lang}/summit/messages"
    ))
    .unwrap();
    for component in components {
        let component_name = component
            .as_ref()
            .unwrap()
            .file_name()
            .into_string()
            .unwrap();
        if component_name != ".svn"
            && component_name != "index.lokalize"
            && component_name != "terms.tbx"
        {
            let component_dir = component.unwrap().path();
            println!("{}", component_name);

            let mut component = Component {
                name: component_name.clone(),
                po_files: vec![],
                stats: TranslationStats::default(),
            };

            let po_files = fs::read_dir(component_dir).unwrap();
            for po_file in po_files {
                let po_file_name = po_file.unwrap().file_name().into_string().unwrap();
                let stats = gather_stats(lang, component_name.as_str(), po_file_name.as_str());

                match stats {
                    Ok(stats) => {
                        let po_file = PoFile {
                            name: po_file_name,
                            stats,
                        };

                        component.po_files.push(po_file);
                        component.stats += stats;
                    }
                    _ => {}
                }
            }

            language.stats += component.stats;
            language.components.push(component);
        }
    }

    language
}
