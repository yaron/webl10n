// SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: AGPL-3.0-or-later

use diesel::prelude::*;
use rocket::serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Queryable, Selectable, Serialize)]
#[diesel(table_name = crate::schema::changes)]
#[serde(crate = "rocket::serde")]
pub struct Change {
    pub id: i32,
    pub data: String,
    pub user_id: i32,
    pub created_at: chrono::NaiveDateTime,
    pub modified_at: chrono::NaiveDateTime,
}

#[derive(Serialize, Deserialize, Default)]
#[serde(crate = "rocket::serde")]
pub struct ChangeEntry {
    pub id: String,
    pub old_translation: String,
    pub translation: String,
    pub old_translation_plural: Option<String>,
    pub translation_plural: Option<String>,
}
