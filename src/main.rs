#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_sync_db_pools;
extern crate serde_json;
use rocket::fairing::AdHoc;
use rocket::fs::{relative, FileServer, NamedFile};

use rocket_oauth2::{HyperRustlsAdapter, OAuth2, OAuthConfig };

pub mod db;
pub mod models;
pub mod routes;
pub mod schema;

#[get("/content/<lang>/<component>/<file>")]
async fn content(lang: &str, component: &str, file: &str) -> Option<NamedFile> {
    NamedFile::open(format!(
        "/home/carl/kde/src/l10n-support/{}/summit/messages/{}/{}",
        lang, component, file
    ))
    .await
    .ok()
}

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("Diesel mysql Stage", |rocket| async {
        rocket
            .attach(db::Database::fairing())
            .attach(AdHoc::on_ignite("Diesel Migrations", db::run_migrations))
            .attach(AdHoc::on_ignite("OAuth Config", |rocket| async {
                let config = OAuthConfig::from_figment(rocket.figment(), "gitlab").unwrap();
                rocket.attach(OAuth2::<models::users::User>::custom(
                    HyperRustlsAdapter::default().basic_auth(false), config)
                )
            }))
            .mount(
                "/api/v1/languages",
                routes![routes::languages::index, routes::languages::get],
            )
            .mount("/api/v1/messages", routes![routes::messages::get])
            .mount("/api/v1/changes", routes![routes::changes::find_change, routes::changes::post_change, routes::changes::find_change_by_id])
            .mount("/api/v1", routes![content, routes::users::userinfo])
            .mount("/", routes![routes::login::gitlab_callback, routes::login::gitlab_login, routes::login::gitlab_login_logged, routes::login::logout])
            .mount("/", FileServer::from(relative!("vue/dist")))
    })
}

#[launch]
fn rocket() -> _ {
    rocket::build().attach(stage())
}
